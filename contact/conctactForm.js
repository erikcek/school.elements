const template = `
  <div class="w-50 mt-3 p-3 text-white opinion c-comment-box">
  <div class="d-flex justify-content-between c-row">
    <h3>
      Name: {{ name }}
    </h3>
    <i>{{ createdDate }}</i>
    </div>
    <p>Content: {{ text }}</p>
    <p>Keyword: {{ keyvord }}</p>
  </div>
`;

const renderOpinion = elem => {
  //   const template = document.getElementById('opinionTemplate').innerHTML;
  const formatedElement = Object.assign({}, elem, {
    createdDate: new Date(elem.createdDate).toLocaleDateString()
  });

  const renderedOpinion = Mustache.render(template, formatedElement);
  return renderedOpinion;
};

const saveData = e => {
  e.preventDefault();
  var elements = document.getElementById('contactForm').elements;
  var opinions = [];
  if (localStorage.contactForm) {
    opinions = JSON.parse(localStorage.contactForm);
  }

  var opinion = {
    name: elements['name'].value,
    email: elements['email'].value,
    pictureUrl: elements['pictureUrl'].value,
    gender: elements['gender'].value,
    text: elements['userOpinion'].value,
    keyvord: elements['keywordsSelect'].value,
    acceptance: elements['personalInfo'].value == 'on' ? true : false,
    createdDate: new Date()
  };

  opinions.push(opinion);
  localStorage.contactForm = JSON.stringify(opinions);
  document.getElementById('contactForm').reset();
  renderOpinions();
};

const renderOpinions = () => {
  const opinions = JSON.parse(localStorage.contactForm);

  if (opinions && Array.isArray(opinions)) {
    const container = document.getElementById('opinionsContainer');

    const content = opinions.reduce((acum, elem) => {
      return acum + renderOpinion(elem);
    }, '');

    container.innerHTML = content;
  }
};

const removeOld = () => {
  const opinions = JSON.parse(localStorage.contactForm);
  const today = new Date();
  const yesterday = today.setDate(today.getDate() - 1);
  const filteredOpinions = opinions.filter(
    item => new Date(item.createdDate) >= yesterday
  );

  localStorage.contactForm = JSON.stringify(filteredOpinions);
  renderOpinions();
};

const form = document.getElementById('contactForm');
form.addEventListener('submit', e => saveData(e));

renderOpinions();
